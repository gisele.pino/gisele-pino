#!/bin/bash
#Nombre y Apellido: Gisele Pino
#Profesor:Sergio Pernas
#Materia: Programacion
#TP 3

# Verificar que el usuario sea root

if [ $UID -ne 0 ]; then
    echo "Ejecute este programa como 'root'."
    exit 1 # cerramos el programa con 'exit status 1'
fi

ERR="Gestor de Permisos RWX para Usuarios y Grupos. Ingrese:\n
\tEn opciones del 1 a 4 escriba $0 + accion (ej: chgown)  + usuario o grupo + fichero(s)  \n
\tEn opcion 5 escriba $0 + accion \n
\tEn opcion 6 escriba $0 + accion + numero de permiso ejemplo 644(lectura escritura para dueño - lectura para grupo - lectura para otros). La busqueda se hara sobre el home del usuario actual\n

Elija una accion:\n
\t1 - chgown\tCambia el dueño del archivo o directorio\n
\t2 - chggrp\tCambia grupo del archivo o directorio\n
\t3 - newgrp\tCrear grupo definido por el usuario\n
\t4 - delgrp\tEliminar grupo definido por el usuario\n
\t5 - listgrp\tListar grupos existentes\n
\t6 - search\tBuscar ficheros segun permisos\n"

if [ -z "$*" ]; then #Si la longitud del argumento es 0 se indica el error y se cierra el programa
    echo -e $ERR 
    exit 1
fi

if [[ $1 == chgown && $# -ge 2 ]]; then #Si la accion ingresada es chown  y se ingresan mas opciones que 2 entonces seguardan en la variable PKG para ser usada y aplicar el comando chown


   PKG="$(echo $* | cut -d ' ' -f 2,3,4,5,6,7,8,9)" #En PKG se guarda la lista completa de argumentos pasados al script, cut genera un espacio tabulado y -f es el rango
   chown $PKG
   exit 
 

elif [[ $1 == chggrp ]]; then
 
    if [[ $# -ge 2 ]]; then
        PKG="$(echo $* | cut -d ' ' -f 2,3,4,5,6,7,8,9)"
        chown :$PKG
        exit
    fi 


elif [[ $1 == newgrp ]]; then

    if [[ $# -ge 2 ]]; then
        PKG="$(echo $* | cut -d ' ' -f 2,3,4,5,6,7,8,9)"
        su - -c "groupadd $PKG"
        exit
    fi
elif [[ $1 == delgrp ]]; then

    if [[ $# -ge 2 ]]; then
        PKG="$(echo $* | cut -d ' ' -f 2,3,4,5,6,7,8,9)"
        su - -c "groupdel $PKG"
        exit
    fi

elif [[ $1 == listgrp ]]; then
        cat /etc/group
        exit
    fi

	if [[ $1 == search && $# -ge 2 ]]; then
	PKG="$(echo $* | cut -d ' ' -f 2,3,4,5,6,7,8,9)"
        find $HOME -perm -$PKG #PKG TIENE QUE TENER UN NUMERO QUE REPRESENTE CONJUNTO DE PERMISOS EJEMPLO 644  
        exit
    fi

else
    echo -e $ERR
    exit 1
fi




