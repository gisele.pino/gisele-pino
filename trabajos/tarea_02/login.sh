#!/bin/bash
#Nombre y Apellido: Gisele Pino
#Profesor:Sergio Pernas
#Materia: Programacion

#Punto 2: Login de usuario

LOGALL=/var/log/LOGIN/loginall.log #Se almacena en un fichero comun el timestamp de inicio de sesion 
#de cada usuario del sistema
LOGUSR=/var/log/LOGIN/$USER.logusr.log #Se almacena en un fichero por cada usuario el timestamp  de
#inicio de sesion


echo ""

echo "Comienza el programa 2"

echo "Se crea directorio var/log/LOGIN para guardar los logs"

su -c "mkdir /var/log/LOGIN" 

echo "Ingrese pass root para mostrar y guardar log de todos los usuarios en los ultmos 7 dias:"

su -c "lastlog -t 7 | tee -a $LOGALL"

echo "Se guardara en /var/log/ el login del usuario individual del sistema, ingrese pass root"

su -c "lastlog --user $USER | tee -a $LOGUSR"

echo "Se exportara la variable $USR_LOGIN a /etc/profile para que todos los usuarios la puedan utilizar"

su -c "echo export USR_LOGIN=\$\(date\) >> /etc/profile" root #comando Lucas Cerica (REVISAR 20/9)

echo "Fin"




