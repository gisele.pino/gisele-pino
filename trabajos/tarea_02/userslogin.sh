#!bin/bash
#Este script registra el logueo de usuarios del sistema
#Nombre y Apellido: Gisele Pino
#Profesor: Sergio Pernas
#Materia: Programacion

REG_LOGIN=/home/gisele/gisele-pino/trabajos/tarea_02/registro_login.txt
REG1=/home/gisele/gisele-pino/trabajos/tarea_02/reg_gisele.txt
REG2=/home/gisele/gisele-pino/trabajos/tarea_02/reg_root.txt
REG3=/home/gisele/gisele-pino/trabajos/tarea_02/reg_pino.txt

echo ""

echo "Se guardara en un txt todos los usuarios logueados que se muestran a continuacion"

echo ""

lastlog | tee -a $REG_LOGIN 

sleep 2

echo ""

echo "Se almacena en un fichero por cada usuario el timestamp del inicio de sesion"

lastlog --user gisele > $REG1
lastlog --user root > $REG2
lastlog --user pino > $REG3


echo "fin"




