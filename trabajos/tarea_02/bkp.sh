#!/bin/bash 
#Este script realiza copias de seguridad del /home del usuario standar del sistema 
#Nombre y Apellido: Gisele Pino 
#Profesor:Sergio Pernas 
#Materia: Programacion

#Punto 1: Crear una copia de bkp del dir de usuario ed cada usuario del sistema

BKPIN=$HOME #directorio que se va copiar 
BKPOUT=/var/backups/homebkp #directorio de destino de la copia 
LOGS=/var/log/$USER/log.txt 
ERR=/var/log/$USER/err.txt
DATE=$(date +%Y-%m-%d-%H%M%S) #cronosellador de cada bkp 
HR_INICIO=$(date +%T) 
HR_FIN=$(date +%T) 

echo "Comienza el programa de copias de seguridad del /home"
echo ""
echo "Crear dir de destino de logs en /var/log/$USER, ingrese root pass:"
su -c "mkdir /var/log/$USER/" #Creamos dir de destino de logs
su -c "echo 'Comienza la copia de seguridad a las $HR_INICIO'  2>>$ERR | tee -a $LOGS"
echo ""  
echo "Crear directorio de destino de bkp, ingrese root pass:"
su -c "mkdir /var/backups/homebkp" #Crea el directorio de bkp 
echo "Compresion de $HOME enviada a dir de destino /var/backups/homebkp, ingrese root pass:" 
su -c "tar -cvzpf $BKPOUT/$USER.$DATE.tar.gz $BKPIN" #comprime y envia el bkp al destino 
echo ""
su -c "echo 'Finaliza la copia de seguridad a las $HR_FIN' 2>>$ERR | tee -a $LOGS"
echo ""
echo "Fin"
