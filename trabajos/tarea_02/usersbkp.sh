#!bin/bash
#Este script realiza copias de seguridad del /home de los usuarios del sistema
#Nombre y Apellido: Gisele Pino
#Profesor: Sergio Pernas
#Materia: Programacion


SRCIN=/home #directorio que se va a respaldar
SRCOUT=/usersbkp #directorio donde se resguarda bkp
LOG=/usersbkp/log.txt #Log con inicio y fin del proceso
HR_INICIO=$(date +%r) #Hora de inicio del proceso
HR_FIN=$(date +%r) #Hora de fin de proceso
REG_TIMESTAMP=/usersbkp/reg_stdin_sterr.txt #Registro de salida de errores

echo ""

echo "Inicia el proceso de backup de carpetas de usuarios del sistema"

echo ""

su -c "echo 'Comienza el backup de usuarios a las' $HR_INICIO | tee -a $LOG" #Ejecutar como root
#para agregar hora de inicio al log

sleep 2

su -c "cp -r $SRCIN $SRCOUT 2>> $REG_TIMESTAMP" #cp copia como root /home en la carpeta de respaldo

su -c "echo 'Finaliza el backup de usuarios a las' $HR_FIN | tee -a $LOG" #Ejecutar como root para agregar registro de hora finalizacion

echo "Done"

